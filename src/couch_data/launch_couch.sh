#!/bin/bash

COUCHDB=couchdb
INI=fedora.ini

if hostname -A | grep madsgroup >/dev/null; then
	# Host needs special couchdb
	echo "Launching as MADSGroup instance"
	COUCHDB=../../../ext/usr/bin/couchdb
	COUCHDB=$(readlink -f $COUCHDB)
	INI=mads.ini
fi

echo "Launching $COUCHDB -a $INI"

export COUCHDB_URI=$(<couch.uri)
export PYTHONPATH=./couchdb-python:$PYTHONPATH

exec env -i COUCHDB_URI=$COUCHDB_URI PYTHONPATH=$PYTHONPATH PATH=$PATH PWD=$PWD SHELL=$SHELL HOME=$HOME HOSTNAME=$HOSTNAME $COUCHDB -a $INI

