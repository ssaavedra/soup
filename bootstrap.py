#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, argparse

parser = argparse.ArgumentParser(description='Bootstrap the project to get a development platform')

parser.add_argument('-i', '--isolated', default=False, action='store_true', help='Also, get all dependencies outside the operating system')
parser.add_argument('--prefix', default='../ext', action='store', help='Path to install isolated packages to')

def bash(script):
    '''Executes script in bash.'''
    # TODO Actual automatic execution to take place
    print "Please, paste this in the bash shell:"
    print script


def get_isolated_packages():

    bash_source='''
    wget http://ftp.cixug.es/apache/couchdb/source/1.5.0/apache-couchdb-1.5.0.tar.gz
    tar -zxf apache-couchdb-1.5.0.tar.gz
    (cd apache-couchdb-1.5.0 && ./configure --prefix=`pwd` && make) &
    wget http://nodejs.org/dist/v0.10.24/node-v0.10.24.tar.gz
    tar -zxf node-v0.10.24.tar.gz
    waitpid
    (cd node-v0.10.24 && ./configure --prefix=`pwd` && make) &
    waitpid
    wget https://npmjs.org/install.sh -O - | bash
    '''

    print 'THIS IS NOT WORKING YET, but they\'re the approximate steps to follow'
    print bash_source

def inform_needed_packages():
    print '''In order to get the development platform, you'll need the following software available in your PATH:
        - Python (you have it if you got here)
        - git (can't be installed automatically)
        - CouchDB
        - node.js
        - npm (the Node Package Manager)
        - kanso (via npm)
    '''


def main():
    args = parser.parse_args()

    if args.isolated:
        get_isolated_packages()
    else:
        inform_needed_packages()

    if False:
        # Not yet ready for bombs
        bash("(cd src && npm install kanso) &)"



if __name__ == '__main__':
    main()
    sys.exit(0)

# vim: set et ts=4 sts=4 sw=4:
